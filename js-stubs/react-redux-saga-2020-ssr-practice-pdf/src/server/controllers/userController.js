import {
	create,
	find,
	updateByEmail,
	deleteByEmail,
} from "../models/userModel";
import express from "express";
import bodyParser from "body-parser";

const userRouter = express.Router();
// userRouter.use(bodyParser.urlencoded({ extended: false }));
userRouter.use(bodyParser.json());

userRouter.post("/login", async () => {});

userRouter.post("/signup", async (req, res) => {
	const findResult = await find({ email: req.body.email });
	console.log("findResult", findResult);
	if (findResult.length) {
		res.send({ message: "User already exists." });
		return;
	}
	const entry = await create({
		...req.body,
		email: req.body.email,
		password: req.body.password,
	});
	res.send(entry);
});

userRouter.post("/update", async (req, res) => {
	const result = await updateByEmail(req.body.email, req.body.data);
	res.send(result);
});

userRouter.post("/delete", async (req, res) => {
	const result = await deleteByEmail(req.body.email);
	res.send(result);
});

export default userRouter;
