import mongoose from "mongoose";

const UserSchema = mongoose.Schema({
	_id: { type: mongoose.Schema.Types.ObjectId, required: true, auto: true },
	email: {
		type: String,
		required: true,
		unique: true,
	},
	password: { type: String, required: true },
});

const User = mongoose.model("Users", UserSchema, "Users");

export const create = async function (obj) {
	const email = obj.email;
	const password = obj.password;
	console.log("User", { ...obj, email, password });
	const user = new User({ ...obj, email, password });
	return await user.save();
};

export const find = async function (obj) {
	const email = obj.email;
	const result = await User.find({ email }).lean();
	return result;
};

export const updateByEmail = async function (email, obj) {
	const result = await User.update({ email }, obj);
	return result;
};

export const deleteByEmail = async function (email) {
	const result = await User.deleteOne({ email });
	return result;
};

export default User;
