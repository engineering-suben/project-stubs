import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

import { homeReducer } from "../pages/home/ducks";

const createRootReducer = (customHistory) =>
	combineReducers({
		// ...exampleReducer
		router: connectRouter(customHistory),
		homeReducer,
	});

// const reducer = combineReducers({
//   // ...exampleReducer
//   homeReducer,
// });

// export default reducer;

export default createRootReducer;
