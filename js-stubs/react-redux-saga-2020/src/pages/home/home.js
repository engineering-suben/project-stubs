import React, { useEffect } from "react";
import { connect } from "react-redux";

import { getLoading, doSomething } from "./ducks";

import "./home.scss";

const Home = ({ loading, doSomething }) => {
	useEffect(() => {
		setTimeout(() => {
			doSomething();
		}, 1000);
	}, []);

	return <div className="home-page">React-Redux-Saga-2020-boilerplate</div>;
};

const enhance = connect(
	(state) => ({
		loading: getLoading(state),
	}),
	{
		doSomething,
	}
);

export default enhance(Home);
